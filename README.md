# DetChar Safety Injeections

A simple module to generate a variety of Hardware Injection files for DetChar Safety studies. These should provide an intuitive user interface to generate arbitrary combinations of a few injection morphologies.

This is accomplished by defining "Injection" objects in waveforms.py which are then called from an executable. Each object is instantiated with a pointer to a config object (SafeConfigParser), from which it extracts the parameters. These objects then standardize how time-series are reported. This also makes extensions to other types of injections extremely simple.

Currently supported Injection subclasses include

  - Zeros
  - SineGaussian
  - Gaussian
  - Whistle
  - Chirp

--------------------------------------------------

# dependencies

  - numpy
