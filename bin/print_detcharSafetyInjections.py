#!/usr/bin/env python
usage       = "detcharSafetyInjections.py [--options]"
description = "a simple script that parses injection config files and produces an h(t) timeseries"
author      = "reed.essick@ligo.org"

#-------------------------------------------------

import waveforms

import sys

import numpy as np

from optparse import OptionParser

#-------------------------------------------------

parser = OptionParser(usage=usage, description=description)

### options about verbosity and output
parser.add_option('-v', '--verbose', default=False, action='store_true')

parser.add_option('-o', '--output-file', default=None, type='string', \
    help='path to an output file for the timeseries. If not supplied, \
output will be printed to stdout.' )

### options about which injections to make
parser.add_option('-c', '--injConfig', nargs=2, default=[], action='append', \
    help='the config file followed by the number of injections of this type. \
eg: "-c SG.ini 5" would schedule 5 injections defined in SG.ini. This option can \
be repeated to specify several series of injections.' ) 

### options about sampling rate and spacing between injections
parser.add_option('', '--fsamp', dest='fsmp', default=16384, type='float', \
    help='the sampling frequency (in Hz) for the timeseries. \
DEFAULT=16384' )

parser.add_option('', '--spacing', dest='tspc', default=5, type='float', \
    help='the amount of time between injections (in sec). Each Injection config \
specifies the duration of each injection of that type, and --spacing is inserted \
between these.\
DEFAULT=5' )

parser.add_option('', '--buffer', dest='tbuf', default=30, type='float', \
    help='the amount of time (in sec) padded to the beginning and end of the injection time series\
filled with zeros. This can ensure that any transients associated with the beginning of \
the injection process are well separated from the actual injection. 
NOTE: --buffer is required to be >= --spacing \
DEFAULT=30' )

opts, args = parser.parse_args()

#-------------------------------------------------

### set up output file
if opts.output_file:
    if opts.verbose:
        print >> sys.stderr, 'writing into : %s'%opts.output_file
    sys.stdout = open(opts.output_file, 'w') ### redirect stdout to --output-file

#-------------------------------------------------

### Instantiate buffers and spacing as "Zeros" waveforms
spc = waveforms.Zeros( opts.tspc*opts.fsmp )
buf = waveforms.Zeros( max(opts.tbuf*opts.fsmp - spc.N, 0) ) ### we require buf>=spc because it makes
                                                             ### algorithmically sipmler to iterate
                                                             ### without special cases
### add buffer to the start
buf.printInj()

### iterate through injections
for configname, num in opts.injConfig:

    ### read in config file
    if opts.verbose:
        print >> sys.stderr, "reading injection config : %s"%configname
    config = SafeConfigParser()
    config.read( configname )

    ### instantiate injection object
    inj = waveform.initInj( config ) 

    ### iterate and print 
    if opts.verbose:
        print >> sys.stderr, "  adding %s injections"%num
    num = int(num)
    while num > 1:
        spc.printInj()                 ### print spacing
        inj.printInj( fsamp=opts.fsmp ) ### print the injection

        num  -= 1

### print the final buffer
spc.printInj()
buf.printInj()
