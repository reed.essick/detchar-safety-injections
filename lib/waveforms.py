description = "a module housing classes that represent several waveforms, each of which know how to convert themselves into timeseries and print themselves in the correct format"
author      = "reed.essick@ligo.org"

#-------------------------------------------------

import sys
import types ### needed to build dictionary to reference commands by name

import numpy as np

#-------------------------------------------------

### useful constants
sqrt2 = 2**0.5

#-------------------------------------------------

def tukeyWindow( N, alpha=0.1 ):
    """
    a generator for a tukey window of length N and parameter alpha
    used to window injections
    """
    raise NotImplementedError('tukeyWindow')


#-------------------------------------------------

class Injection(object):
    """
    a parent class for injection morphologies. Defines some standard methods
    """
    name = "Injection"

    def __init__(self, config):
        self.config = config

    def genSeries(self, fsamp=16384):
        """
        returns a generator that will produce the time-series for this injection
        """
        pass ### this method is for syntactic completeness and immediately ends the iteration

    def printInj(self, fsamp=16384, file_obj=sys.stdout):
        """
        standardizes how output is reported (number of significant figures, format, etc)
        delegates to self.genSeries to compute the waveform.
        """
        for val in self.genSeries( fsamp=fsamp ):
            print >> file_obj, "%.9e"%(val)

#------------------------

class Zeros(Injection):
    """
    a sequence of zeros. Useful for spacing and buffers
    """
    name = "Zeros"

    def __init__(self, N):
        '''
        overwrites the parent's __init__ because this is a special case of an extremely simple waveform
        '''
        self.N = N

    def genSeries(self, *args, **kwargs):
        """
        overwrites parent method; still produces a generator that yields self.N zeros

        NOTE: we capture args and kwargs even though they're not used to allow calls to this to use an arbitrary signature
        """
        i = 0
        while i < self.N: ### simply yield N zeros
            yield 0.0
            i += 1

#------------------------

class SineGaussian(Injection):
    """
    a SineGaussian injection
    """
    name = 'SineGaussian'

    def genSeries(self, fsamp=16384):
        """
        overwrites parent method; still produces a generator reprsenting the waveform as a timeserie

        h(t) = amplitude * np.exp( -(t/tau)**2 ) * np.sin( 2*pi*central_freq*t + phi )
        """
        dt = 1./fsamp

        ### extract parameters from config
        dB  = self.config.getfloat('duration', 'truncate dB')

        A   = self.config.getfloat('intrinsic', 'amplitude')
        fc  = self.config.getfloat('intrinsic', 'central freq')
        Q   = self.config.getfloat('intrinsic', 'quality')
        phi = self.config.getfloat('intrinsic', 'phase')

        ### compute Gaussian width
        tau = Q/(sqrt2*np.pi*f)

        ### figure out duration of this injection based on truncate dB
        T = tau*(-np.log(10**dB))**0.5
        N = np.ceil(T/dt) ### figure out how many samples this is
        T = N*dt ### round up...

        ### actually step through the waveform
        t = -T
        angFreq = twopi*fc
        while t <= T:
            yield A * np.exp( -(t/tau)**2 ) * np.sin( angFreq*t + phi )
            t += dt

#------------------------

class Gaussian(Injection):
    """
    a Gaussian injection
    """
    name = 'Gaussian'

    def genSeries(self, fsamp=16384):
        """
        overwrites parent method; still produces a generator reprsenting the waveform as a timeserie

        h(t) = amplitude * np.exp( -(t/tau)**2 )
        """
        dt = 1./fsamp

        ### extract parameters from config
        dB  = self.config.getfloat('duration', 'truncate dB')

        A   = self.config.getfloat('intrinsic', 'amplitude')
        tau = self.config.getfloat('intrinsic', 'tau')

        ### figure out duration of this injection based on truncate dB
        T = tau*(-np.log(10**dB))**0.5
        N = np.ceil(T/dt) ### figure out how many samples this is
        T = N*dt ### round up...

        ### actually step through the waveform
        t = -T
        while t <= T:
            yield A * np.exp( -(t/tau)**2 )
            t += dt

#------------------------

class Whistle(Injection):
    """
    an injection resembling a RF whistles/arches/beat-notes
    """
    name = "Whistle"

    def genSeries(self, fsamp=16384):
        """
        overwrites parent method; still produces a generator reprsenting the waveform as a timeserie

        h(t) = ???? DEFINE WAVEFORM HERE !!!!
        """
        dt = 1./fsamp

        ### extract parameters from config
        tukeyLength = config.getfloat('duration', 'tukeyLength')
        tukeyAlpha  = config.getfloat('duration', 'tukeyAlpha')

        A   = config.getfloat('intrinsic', 'amplitude')
        F   = config.getfloat('intrinsic', 'maxFreq')
        P   = config.getfloat('intrinsic', 'period')
        phi = config.getfloat('intrinsic', 'phase')

        ### figure out duration of this injection

        ### actually step through the waveform

        raise NotImplementedError()

#------------------------

class Chirp(Injection):
    """
    an injection resembling a chirp
    """
    name = "Chirp"

    def genSamp(self, fsamp=16384):
        """
        overwrites parent method; still produces a generator reprsenting the waveform as a timeseries

        h(t) = ???? DEFINE WAVEFORM HERE !!!!
        """
        dt = 1./fsamp

        ### extract parameters from config
        tukeyLength = config.getfloat('duration', 'tukeyLength')
        tukeyAlpha  = config.getfloat('duration', 'tukeyAlpha')

        Mc   = config.getfloat('intrinsic', 'chirpMass')
        Fmax = config.getfloat('intrinsic', 'maxFreq')
        Fmin = config.getfloat('intrinsic', 'minFreq')

        ### figure out duration of this injection

        ### actually step through the waveform

        raise NotImplementedError()

#-------------------------------------------------
### a simple helper function to instantiate objects automatically
#-------------------------------------------------

### define dictionary of known Injection classes
__injDict__ = dict( (x.name, x) for x in vars().values() if isinstance(x, type) and issubclass(x, Injection) )

def initInj( config ):
    """
    generates the correct Injection object based on config object
    """
    ### extract injection name from config 
    name = config.get('general', 'name')

    ### return the injection object
    if name in __injDict__: 
        return __injDict__[name]( config )

    else:
        raise ValueError('could not find Injection object with name=%s'%name)
